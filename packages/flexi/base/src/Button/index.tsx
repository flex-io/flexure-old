import React from "react";

export const BaseButton: React.FC = ({ children, ...props }) => {
	return <button {...props}>{children} Now!</button>;
};
