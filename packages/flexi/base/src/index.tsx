import styled from "./Styled";

export { styled };
export { BaseButton } from "./Button";
export type { ITheme } from "./Styled";
