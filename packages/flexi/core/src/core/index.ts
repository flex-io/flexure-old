export { Flexite } from "./flexite";
export { FlexComponent, Flexton } from "./flexton";

export type { IFlexTree, IFlexNode } from "./flexite";
export type { IContextCallback } from "./flexton";

// export { Flexite } from "./core/flexite";
