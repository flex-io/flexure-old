import { Flexton } from "./flexton";
import _ from "lodash";
// import { Builder } from "./internal";

interface Properties {
	icon?: any;
	component?: string;
	name?: string;
	path?: string;
	isExact?: boolean;
}

export interface IFlexNode {
	icon?: any;
	component?: any;
	name: string;
	displayName?: string;
	path?: string;
	isExact?: boolean;
	nodes?: IFlexNode[];
	[key: string]: any;
}
export interface IFlexTree {
	name: string;
	icon?: any;
	component: any;
	nodes?: IFlexNode[];
}

export abstract class IFlexite {
	abstract props: Properties;
	public abstract children: IFlexite[];
	protected parent: IFlexite | any = undefined;

	public setParent(parent: IFlexite | any) {
		this.parent = parent;
	}

	public getParent(): IFlexite {
		return this.parent;
	}

	public abstract add(component: IFlexite): void;

	public abstract remove(component: IFlexite): void;

	public isComposite(): boolean {
		return false;
	}

	public abstract operation(): { [key: string]: any };
}

// export class Leaf extends Component {
// 	props: Properties = {};

// 	public operation(): { [key: string]: any } {
// 		return { leaf: "Leaf" };
// 	}
// }

export class FlexComposite extends IFlexite {
	props: Properties = {};
	public children: IFlexite[] = [];

	public add(component: IFlexite): void {
		this.children.push(component);
		component.setParent(this);
	}

	public remove(component: IFlexite): void {
		const componentIndex = this.children.indexOf(component);
		this.children.splice(componentIndex, 1);

		component.setParent(null);
	}

	public isComposite(): boolean {
		return true;
	}

	public operation(): { [key: string]: any } {
		const results: any = this.children.length > 0 ? {} : undefined;
		this.children.forEach((child) => {
			_.assign(results, child.operation());
		});

		return { [`${this.props.name}`]: { ...this.props, nodes: results } };
	}

	public initialize(arg: IFlexTree) {
		const builder = new Builder<FlexComposite>(this);
		this.setParent(builder.buildApp(arg));
	}
}

export const Flexite = new FlexComposite();

/**
 *
 *
 *
 *
 * 				BUILDER
 *
 *
 *
 *
 */

interface IBuilder {
	buildApp(args: any): void;
}

export class Builder<T extends IFlexite> implements IBuilder {
	private product: T;

	constructor(parent: T) {
		this.product = parent;
	}

	protected addApp(parent: IFlexite, child: IFlexNode) {
		const _child = new FlexComposite();
		let component: any = child.component ? Flexton.register(child.name, child.component) : undefined;
		_child.props = {
			...child,
			component,
			name: `${parent.props.name}_${child.name}`,
			path: (parent.props.path ? parent.props.path : "").concat(child.path ? child.path : ""),
		};

		

		child.nodes?.forEach((node) => {
			// note: Register a path to Flexton
			//node.component && node.path && Flexton.registerPath({ ...node, component: node.name });

			this.addApp(_child, node);
		});
		// this.cleanChild(_child);
		parent.add(_child);
		Flexton.registerContext(parent.operation());
	}

	// private cleanChild(child: any) {
	// 	child.children.map((node: any) => {
	// 		this.cleanChild(node);
	// 	});

	// 	child.props = { ...child.props, path: child.props.component ? child.props.path : undefined };
	// }

	public buildApp(tree: IFlexTree): T {
		this.product.props.name = tree.name;
		this.product.props.component = Flexton.register(tree.name, tree.component);

		tree.nodes?.forEach((node) => {
			this.addApp(this.product, node);
		});

		Flexton.registerContext(this.product.operation());
		// console.log(this.product.operation())
		return this.product;
	}

	public getProduct(): T {
		const result = this.product;
		return result;
	}
}
