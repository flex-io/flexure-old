# flexure

> flexure lib

[![NPM](https://img.shields.io/npm/v/flexure.svg)](https://www.npmjs.com/package/flexure) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save flexure
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'flexure'
import 'flexure/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [rslmanzano](https://github.com/rslmanzano)
