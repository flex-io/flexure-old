/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import React from "react";
import { Icon } from "react-icons-kit";
import { arrows_squares } from "react-icons-kit/linea/arrows_squares";

const AppBarNavBarStyle = {
	_container: css`
		color: #172b4d;
		font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Fira Sans",
			"Droid Sans", "Helvetica Neue", sans-serif;
		font-size: 14px;
		font-style: normal;
		font-weight: 400;
		line-height: 1.42857142857143;
		text-decoration-skip-ink: auto;
		margin: 0;
		padding: 0;
		display: flex;
		flex-direction: column;
	`,
};

interface IAppNavBarStyle {
	_container?: any;
	_nearItems?: any;
	_farItems?: any;
}

interface Props {
	_style?: IAppNavBarStyle;
}

export const AppNavBar: React.FC<Props> = ({ _style }) => {
	return (
		<div css={_style?._container}>
			<div css={_style?._nearItems}>
				<NavBarTitle>Title</NavBarTitle>
				<Icon icon={arrows_squares} />
				<Icon icon={arrows_squares} />
			</div>

			<div css={_style?._farItems}>
				<Icon icon={arrows_squares} />
				<Icon icon={arrows_squares} />
			</div>
		</div>
	);
};

AppNavBar.defaultProps = {
	_style: AppBarNavBarStyle,
};

const NavBarTitle: React.FC = ({ children }) => {
	return <div>{children}</div>;
};
