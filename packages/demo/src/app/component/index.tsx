import React from "react";
import { AppNavBar } from "./NavBar";

export const AppComponent: React.FC = ({ children }) => {
	return (
		<>
			<AppNavBar />
		</>
	);
};

// export const AppComponent: React.FC = ({ children }) => {
// 	return (
// 		<>
// 			<Container>
// 				<AppNavBar />
// 				<Content>
// 					<Page>
// 						<Menu>
// 							<Menu.title />
// 							<Menu.nav />
// 							<Menu.section>
// 								<Menu.item />
// 							</Menu.section>
// 						</Menu>
// 						{children}
// 					</Page>
// 				</Content>
// 				<Sidebar />
// 			</Container>
// 		</>
// 	);
// };
