import React from "react"
import { IFlexTree } from "@flexi/core"
import { AppComponent } from "./component"

const Overview: React.FC = () => {
	return <div>Overview</div>
}

const ProjectList: React.FC = () => {
	return <div>Project List</div>
}

const App: IFlexTree = {
	name: "app",
	component: AppComponent,
	nodes: [
		{
			name: "feature",
			nodes: [
				{
					name: "dashboard",
					path: "/dashboard",
					nodes: [
						{ name: "overview", displayName: "Overview", path: "/overview", component: Overview },
						{ name: "repositories", displayName: "Repositories" },
						{
							name: "projects",
							displayName: "Projects",
							path: "/projects",
							nodes: [{ name: "list", displayName: "List", path: "/list", component: ProjectList }],
						},
						{ name: "pull_requests", displayName: "Pull requests" },
						{ name: "snippets", displayName: "Snippets" },
					],
				},
				{
					name: "projects",
					path: "/projects",
					nodes: [
						{ name: "overview", displayName: "Overview", path: "/overview", component: Overview },
						{ name: "repositories", displayName: "Repositories" },
						{ name: "projects", displayName: "Projects" },
						{ name: "pull_requests", displayName: "Pull requests" },
						{ name: "snippets", displayName: "Snippets" },
					],
				},
			],
		},
		{
			name: "plugin",
			nodes: [
				{ name: "chat", icon: "chatIcon" },
				{ name: "fileManager", icon: "fileIcon" },
			],
		},
		{
			name: "offCanvas",
			nodes: [
				{ name: "title", icon: "SearchIcon" },
				{ name: "search", icon: "SearchIcon" },
				{ name: "new", icon: "SearchIcon" },
				{ name: "products", icon: "SearchIcon" },
				{ name: "help", icon: "SearchIcon" },
				{ name: "user", icon: "SearchIcon" },
			],
		},
	],
}

export default App
