import React from "react";

type Props = {
	title: React.ReactNode;
	nearItems?: Array<any>;
	farItems?: Array<any>;
};

export const AppNavBar: React.FC<Props> = (props) => {
	return (
		<div>App NavBar</div>
	);
};
