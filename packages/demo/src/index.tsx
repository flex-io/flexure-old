import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { Flexite, FlexComponent, Flexton } from "@flexi/core";
import App from "./app";
import { BaseButton } from "@flexi/base";

Flexite.initialize(App);
console.log(Flexton.context);
ReactDOM.render(
	<React.StrictMode>
		<FlexComponent name="app" />
		<BaseButton>Click me</BaseButton>
	</React.StrictMode>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
